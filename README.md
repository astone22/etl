# README #

Read the datasource schema from XML file created with projectprototype (CreateXML.java) and then insert the schema into PostgreSQL remote db that holds the schema of the datasource. 

ETL.java will insert the tables, columns, primary keys, into PostgreSQL remote database to hold the schema of the datasource.

ETL.java also has the ability to synchronize and update tables, columns, primary keys, that have been changed in the datasource.

Setting up the ability to create a HSQLDB with this schema, then ETL1 and ETL2 will actually transfer the data.