

import javax.xml.parsers.*;
import javax.xml.xpath.*;

import java.io.*;

import org.postgresql.util.PSQLException;
import org.w3c.dom.*;
import org.xml.sax.*;

import java.sql.*;
import java.util.*;


/**
 * 
 * @Author Alan Stone
 *
 */

public class Etl {
	
	
	//getting parameter to determine if transferring only person table or child tables, or persontable with only unidentified fields
	
	public static void main(String[] args){
	try{
		long startTime = System.currentTimeMillis();

		Etl etl = new Etl();
		etl.Etl();
		long stopTime = System.currentTimeMillis();
	    long elapsedTime = stopTime - startTime;
	    System.out.println("Total time spent: "+elapsedTime);
	}
	catch(Exception e){
		
		e.printStackTrace();
	}
	}
	
	public void Etl(){
		System.out.println("------START OF ETL.JAVA-------");

		//Read tableSchema.properties to find the name of the xml file with the schema
		try {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
        Properties tableSchemaProp = new Properties();
        String tableSchemaFileName = "";
        try {
            tableSchemaProp.load(new FileInputStream("webapps/Pathr/WEB-INF/files/tableSchema.properties"));
            tableSchemaFileName = tableSchemaProp.getProperty("filename");
            System.out.println("Found xml filename from tableSchema.properties");
            System.out.println("Now loading: " + tableSchemaFileName);
        } catch (IOException e) {
            System.out.println("File tableSchema.properties is not found");
            e.printStackTrace();
            System.exit(1);
        }
		tableSchemaProp = null;
		Properties prop = new Properties();
        String userName = "", password = "", location = "", schemaoutput = "";
        
        
        //Get the connection info from data.properties for connecting to chin_remote
        try {
            prop.load(new FileInputStream("webapps/Pathr/WEB-INF/files/data.properties"));
			location = prop.getProperty("location");
            userName = prop.getProperty("user");
            password = prop.getProperty("password");
            schemaoutput = prop.getProperty("schemaoutput");
            
        } catch (IOException ex) {
            System.out.println("File data.properties not found");
            ex.printStackTrace();
            System.exit(1);
        }
		prop = null;
		System.gc();
        //make connection to chin_remote (location, userName, password - are from data.properties)
        Connection connection = DriverManager.getConnection(location, userName, password);
        
        Statement statement = connection.createStatement();
		
		//OPENS /output/.xml file into document
        File file = new File(schemaoutput + "\\" + tableSchemaFileName);
        Document document = docBuilder.parse(file);
        System.out.println("Successfully loaded: " + tableSchemaFileName);

        
        
        
        XPathFactory xpFactory = XPathFactory.newInstance();
        XPath xPath = xpFactory.newXPath();
        document.getDocumentElement().normalize();
        //System.out.println("Root element: " + document.getDocumentElement().getNodeName());
        ResultSet resultSet = statement.executeQuery("select persontable from chin_remote_data_source;");
        resultSet.next();
        String persontable = resultSet.getString(1); //there will be only one result
        System.out.println("person table is: " + persontable);
        
//        XPathExpression expr = xPath.compile("//database/tables/table[@type='TABLE']"); //looking for table tag with type=table only
        XPathExpression expr = xPath.compile("//database/tables/table"); //looking for table tag 

        NodeList  nList = (NodeList) expr.evaluate(document, XPathConstants.NODESET); //
//        NodeList nList = document.getElementsByTagName("table");
        
        
        

        NodeList nList1 = document.getElementsByTagName("primaryKey");
        //Node nNode1 = nList1.item(0);
        //String sTableName1 = ((Element) nNode1).getAttribute("column");
        //System.out.println("Test: " + sTableName1);
        
        
        
        
//        int numberOfColumnsXML = Integer.parseInt(xPath.evaluate("count(//database/tables/table[@type='TABLE']/column)", document));
        int numberOfColumnsXML = Integer.parseInt(xPath.evaluate("count(//database/tables/table/column)", document));

        System.out.println("Number of columns in //database//tables//table//column: " + numberOfColumnsXML);
        
        resultSet = statement.executeQuery("SELECT count(*) FROM chin_remote_metadatafields;");
        resultSet.next();
        int numberOfColumnsDB = resultSet.getInt(1);
        resultSet = statement.executeQuery("SELECT count(*) FROM chin_remote_tables_ui;");
        resultSet.next();
        int numberOfTablesDB = resultSet.getInt(1);
        
        System.out.println("Number of columns in chin_remote_metadatafields: " + numberOfColumnsDB);
        
        
        
        
        statement = connection.createStatement();
        resultSet = statement.executeQuery("SELECT id FROM chin_remote_data_source;");
        //move cursor forward to the first spot
        resultSet.next();
        int sqlExtractDatasetID = resultSet.getInt(1);
        
        
        //
        //	Insert data to database if there is currently no data in database
        //
    	boolean is_table_active;
        if (numberOfColumnsDB == 0) {
        	System.out.println("Analyzing database: chin_remote_metadatafields has no data in it..");
            statement = connection.createStatement();
            
            resultSet = statement.executeQuery("SELECT df_mdataid FROM chin_remote_metadata_header_tables;");
            resultSet.next();
			int j;
			try{
				j = resultSet.getInt(1);
			}
			catch(SQLException ex){
				j = 0;
			}
            
            for (int temp = 0; temp < nList.getLength(); temp++) {
            	
                Node nNode = nList.item(temp);
                Node nNode1 = ((Element) nNode).getElementsByTagName("primaryKey").item(0);
//                Node nNode1 = nList1.item(temp);
                
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                	statement = connection.createStatement();
                    String sTableName = ((Element) nNode).getAttribute("name");
                    
                    int tType = 1; //1 is table, 2 is view
                    is_table_active = true;
                    if( ((Element) nNode).getAttribute("type").equals("VIEW")) {
                    	tType = 2;
                    	is_table_active = false;
                    }
                    String primaryKey = "";
                    try{
                    primaryKey = ((Element) nNode1).getAttribute("column");
                    System.out.println("Primary Key for table: " + sTableName + " is " + primaryKey);
                    }
                    catch(NullPointerException npe){
                    	//npe.printStackTrace();               
                    }
					//Insert data into chin_remote_tables_ui
                    if (primaryKey.equals("")) {
                    	statement.executeUpdate("INSERT INTO chin_remote_tables_ui(table_name, df_id, is_table_active, type) VALUES (\'" + sTableName + "\', \'" + sqlExtractDatasetID + "\', \'" + is_table_active + "\', \'" + tType + "\');");
                    }
                    else {
                    	statement.executeUpdate("INSERT INTO chin_remote_tables_ui(table_name, table_primary_key, df_id, is_table_active, type) VALUES (\'" + sTableName + "\', \'" + primaryKey + "\', \'" + sqlExtractDatasetID + "\', \'" + is_table_active + "\' , \'" + tType + "\');");
                    }
                    resultSet = statement.executeQuery("SELECT table_id FROM chin_remote_tables_ui WHERE table_name = \'" + sTableName + "\';");
                    resultSet.next();
                    int tableId = resultSet.getInt(1);
                    Element eElement = (Element) nNode;
                    NodeList columnList = eElement.getElementsByTagName("column");
                    
                    //NodeList dataTypeList = eElement.getElementsByTagName("dataType");
//                    System.out.println("TableName: " + eElement.getAttribute("name"));
                    for (int i = 0; i < columnList.getLength(); i++) {
                        statement = connection.createStatement();
                        Element column = (Element)columnList.item(i);
						int sqlType = 0;
						
						//comparing attribute 'type' to for df_translator field
                        if(column.getAttribute("type").equalsIgnoreCase("integer") ||
							column.getAttribute("type").equalsIgnoreCase("int") ||
							column.getAttribute("type").equalsIgnoreCase("int4") ||
							column.getAttribute("type").equalsIgnoreCase("smallint") ||
							column.getAttribute("type").equalsIgnoreCase("tinyint") ||
							column.getAttribute("type").equalsIgnoreCase("int identity") ||
							column.getAttribute("type").equalsIgnoreCase("counter")){
                            sqlType = 5;
                        }
                        else if(column.getAttribute("type").equalsIgnoreCase("character varying") ||
                                column.getAttribute("type").equalsIgnoreCase("text") ||
								column.getAttribute("type").equalsIgnoreCase("ntext") ||
                                column.getAttribute("type").equalsIgnoreCase("character") ||
                                column.getAttribute("type").equalsIgnoreCase("varchar") ||
                                column.getAttribute("type").equalsIgnoreCase("longchar") ||
								column.getAttribute("type").equalsIgnoreCase("nvarchar") ||
								column.getAttribute("type").equalsIgnoreCase("nvarchar2") ||
								column.getAttribute("type").equalsIgnoreCase("nchar") ||
								column.getAttribute("type").equalsIgnoreCase("char") ||
								column.getAttribute("type").equalsIgnoreCase("varchar2") ||
								column.getAttribute("type").equalsIgnoreCase("raw")){
                            sqlType = 12;
                        }
                        else if(column.getAttribute("type").equalsIgnoreCase("boolean") ||
                        		column.getAttribute("type").equalsIgnoreCase("bool") ||
								column.getAttribute("type").equalsIgnoreCase("bit")){
                            sqlType = -7;
                        }
                        else if(column.getAttribute("type").equalsIgnoreCase("bigint")){
                            sqlType = 4;
                        }
                        else if(column.getAttribute("type").equalsIgnoreCase("datetime") ||
								column.getAttribute("type").equalsIgnoreCase("smalldatetime") ||
								column.getAttribute("type").equalsIgnoreCase("datetime2") ||
                                column.getAttribute("type").equalsIgnoreCase("timestamp") ||
								column.getAttribute("type").equalsIgnoreCase("timestamp(6)")){
                            sqlType = 93;
                        }
                        else if(column.getAttribute("type").equalsIgnoreCase("double")){
                            sqlType = 8;
                        }
                        else if(column.getAttribute("type").equalsIgnoreCase("decimal") ||
                                column.getAttribute("type").equalsIgnoreCase("numeric") ||
								column.getAttribute("type").equalsIgnoreCase("number") ||
								column.getAttribute("type").equalsIgnoreCase("float") ||
								column.getAttribute("type").equalsIgnoreCase("real")){
                            sqlType = 2;
                        }
						else if(column.getAttribute("type").equalsIgnoreCase("date")){
                            sqlType = 91;
                        }
                        
                        //insert data into chin_remote_metadatafields table
                        statement.executeUpdate("INSERT INTO chin_remote_metadatafields (df_translator_field_sqltype, df_mdataid, df_name, df_type, sql_extract_dataset_id, sql_extract_field_name, sql_extract_table_id) VALUES(" + sqlType + ", " + (j++) + ", \'" + column.getAttribute("name") + "\', \'" + column.getAttribute("type") + "\', " + sqlExtractDatasetID + ", \'" + column.getAttribute("name") + "\', " + tableId + ");");

//                        statement.executeUpdate("INSERT INTO chin_remote_metadatafields (df_mdataid, df_name, df_type, sql_extract_dataset_id, sql_extract_field_name, sql_extract_table_id) VALUES(" + (j++) + ", \'" + columnList.item(i).getTextContent() + "\', \'" + column.getAttribute("type") + "\', " + sqlExtractDatasetID + ", \'" + columnList.item(i).getTextContent() + "\', " + tableId + ");");

//                        System.out.println("FieldName: " + columnList.item(i).getTextContent());
//                        System.out.println("type: " + column.getAttribute("type"));
                    }
                }
            }
            System.out.println("Data successfully transferred.");
            connection.close();
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        //if data exists in database do this
        } else {
        	
        	
       //
       //	SYNCHRONIZATION - COMPARE TABLES IN XML AND DB
       //
        	
        	//
        	//	SETUP VARIABLES FOR COMPARISON FOR TABLE SYNCHRONIZATION
        	//
        	int numberOfTablesXML = 0;
        	for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                	String sTableName = ((Element) nNode).getAttribute("name");
                    if( ((Element) nNode).getAttribute("type").equals("TABLE") || ((Element) nNode).getAttribute("type").equals("VIEW"))
                    	numberOfTablesXML++;
                }
        	}
        	String[] tableNameFromXML = new String[numberOfTablesXML];
        	for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                	tableNameFromXML[temp] = ((Element) nNode).getAttribute("name");
                }
        	}
        	System.out.println("Number of tables in XML: " + numberOfTablesXML);
        	System.out.println("Number of tables in DB: " + numberOfTablesDB);
        	statement = connection.createStatement();
        	resultSet = statement.executeQuery("SELECT table_name FROM chin_remote_tables_ui;");
        	String[] tableNameFromDB = new String[numberOfTablesDB];
        	int count = 0;
        	while(resultSet.next()){
        		tableNameFromDB[count] = resultSet.getString("table_name");
        		count++;
        	}
        	     	
        	
        	
        	
        	
           	
//        	//
//        	//	GET PRIMARY KEY AND TYPE ATTRIBUTES FOR INSERTING TO DB IF NEEDED BY SYNC
//        	//
//        	
//        	List<String> primaryKeyList = new ArrayList<>();
//        	List<Integer> typeList = new ArrayList<>();
//        	String[] primaryKeyArray = new String[numberOfTablesXML];
//        	Integer[] typeArray = new Integer[numberOfTablesXML];
//
//        	for (int temp = 0; temp < nList.getLength(); temp++) {
//            	
//                Node nNode = nList.item(temp);
//                Node nNode1 = ((Element) nNode).getElementsByTagName("primaryKey").item(0);
//                
//                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
//
//                    int tType = 1; //1 is table, 2 is view
//                    if( ((Element) nNode).getAttribute("type").equals("VIEW")){
//                    	tType = 2;
////                    	typeList.add(tType);
////                    	System.out.println(tType);
//                    }
//                    try{
////                      primaryKeyArray[temp] = ((Element) nNode1).getAttribute("column");
////                      System.out.println("primaryKeyArray[" + temp + "] is " + primaryKeyArray[temp]);
////                      System.out.println("typeArray[" + temp + "] is " + typeArray[temp]);
//                    	primaryKeyList.add(((Element) nNode1).getAttribute("column"));
////                    System.out.println("primaryKeyList " + primaryKeyList.get(temp));
////                    System.out.println("typeList " + typeList.get(temp));
//                    }
//                    catch(NullPointerException npe){
//                    	System.out.println("Primary Key is empty");                
//                    }
//        	}

        	
        	
        	//
        	//	COMPARISON:
        	//  FIND TABLES FROM XML THAT AREN'T YET IN DB - STORE THEM IN 'newTables' ARRAY LIST
        	//
        	List<String> XMLList = new ArrayList<>(numberOfTablesXML);
        	for (int i=0;i<numberOfTablesXML;i++){
        		XMLList.add(tableNameFromXML[i]);
        	}
        	List<String> DBList = new ArrayList<>(numberOfTablesDB);
        	for (int i=0;i<numberOfTablesDB;i++){
        		DBList.add(tableNameFromDB[i]);
        	}
        	boolean matchFound = false;
        	int countTemp = 0;
        	System.out.println("XML List: " + XMLList);
        	System.out.println("DB List: " + DBList);
        	List<String> newTables = new ArrayList<>();
        	for (String i : XMLList) {
        		for (String j : DBList) {
        			if (i.equalsIgnoreCase(j)) {
        				matchFound = true;
        				break;
        						}
        					}
        		if (!matchFound) {
        			newTables.add(i);
        			
        			
        			
        						}
        		matchFound = false;
        		countTemp++;
        	}
        	
        	
        	
        	//
        	//	GRAB PRIMARY KEY/TYPE FOR NEW TABLE (newTables ARRAYLIST) AND INSERT MISSING TABLES/KEY/TYPE INTO DB
        	//

        	for (int ai = 0; ai < nList.getLength(); ai++) {
        		for (int a = 0; a < numberOfTablesXML; a++) {
	        			
	                Node nNode3 = nList.item(ai);
	                Node nNode4 = ((Element) nNode3).getElementsByTagName("table").item(0);
	                Node nNode5 = ((Element) nNode3).getElementsByTagName("primaryKey").item(0);
	                
	                if (nNode3.getNodeType() == Node.ELEMENT_NODE) {
	
	                	String sPrimaryKey = "";
	                    String sTestTableName = ((Element) nNode3).getAttribute("name");
	                    String sTestTableType = ((Element) nNode3).getAttribute("type");
	                    int tType = 1; //1 is table, 2 is view
	                    is_table_active = true;
	                    if(sTestTableType.equals("VIEW")){
	                    	tType = 2;
	                    	is_table_active = false;
	                    }
	                    try{
	                        sPrimaryKey = ((Element) nNode5).getAttribute("column");
	                        }
	                        catch(NullPointerException npe){
	                        	//npe.printStackTrace();               
	                        }
	                    try{
//	                    	System.out.println("New Table: " + newTables.get(a));
		                    if (sTestTableName.equalsIgnoreCase(newTables.get(a))) {
		                    	//System.out.println(sTestTableName + " MATCHES " + newTables.get(a) + "!!!");
		                    	//System.out.println("INSERT INTO chin_remote_tables_ui(table_name, df_id, table_primary_key, type) VALUES (\'" + newTables.get(a) + "\', \'" + sqlExtractDatasetID + "\', \'" + sPrimaryKey + "\', \'" + tType + "\');");
		                    	statement = connection.createStatement();
		                		try {
		                			if (sPrimaryKey.equals("")) {
		                				statement.executeQuery("INSERT INTO chin_remote_tables_ui(table_name, df_id, is_table_active, type) VALUES (\'" + newTables.get(a) + "\', \'" + sqlExtractDatasetID + "\', \'" + is_table_active + "\', \'" + tType + "\');");
		                			}
		                			else {
		                				statement.executeQuery("INSERT INTO chin_remote_tables_ui(table_name, df_id, is_table_active, table_primary_key, type) VALUES (\'" + newTables.get(a) + "\', \'" + sqlExtractDatasetID + "\', \'" + is_table_active + "\', \'" + sPrimaryKey + "\', \'" + tType + "\');");
		                			}
		                		}
		                    	catch(PSQLException e) {
		                    		//e.printStackTrace();
		                    	}
		                    	
		                    }
	                    }
	                    catch (IndexOutOfBoundsException e) {
	                    	//e.printStackTrace();
	                    }
	                    

	                }
                }
        	}
        		
        		
        		

        	
        	
 
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	//
        	//	COMPARE AND SYNC FIELDS IN XML AND DB
        	//
        	
        	System.out.println("Analyzing database: chin_remote_metadatafields currently has data in it..");
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT chin_remote_metadatafields.df_mdataid, chin_remote_metadatafields.sql_extract_field_name, chin_remote_metadatafields.df_translator_field_sqltype, chin_remote_tables_ui.table_name FROM chin_remote_metadatafields, chin_remote_tables_ui WHERE chin_remote_metadatafields.sql_extract_table_id = chin_remote_tables_ui.table_id ORDER BY chin_remote_metadatafields.df_mdataid;");


//            resultSet = statement.executeQuery("SELECT * FROM chin_remote_metadatatranslator ORDER BY df_mdataid;");
            String[] fieldNamesFromDatabase = new String[numberOfColumnsDB];
            int[] dataTypesFromDatabase = new int[numberOfColumnsDB];
            String[] tableNamesFromDatabase = new String[numberOfColumnsDB];
            boolean[] isApproved = new boolean[numberOfColumnsDB];
//            System.out.println("numberOfColumnsDB = " + numberOfColumnsDB);
            int row = 0;
            int orderNumber = 0;
            while (resultSet.next()) {
                orderNumber = resultSet.getInt("df_mdataid");
                fieldNamesFromDatabase[row] = resultSet.getString("sql_extract_field_name");
                dataTypesFromDatabase[row] = resultSet.getInt("df_translator_field_sqltype");
                tableNamesFromDatabase[row] = resultSet.getString("table_name");
                row++;
//                System.out.println(fieldNamesFromDatabase[row - 1] + " " + dataTypesFromDatabase[row - 1] + " " + tableNamesFromDatabase[row - 1]);
            }

            String[] fieldNamesFromXML = new String[numberOfColumnsXML];
            String[] dataTypesFromXML = new String[numberOfColumnsXML];
            String[] tableNamesFromXML = new String[numberOfColumnsXML];
            int i = 0, k = 0;
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

//                System.out.println("\nCurrent Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String sTableName = eElement.getAttribute("name");

                    NodeList columnList = eElement.getElementsByTagName("column");
//                    NodeList dataTypeList = eElement.getElementsByTagName("dataType");
//                    System.out.println("TableName: " + eElement.getAttribute("name"));
                    int j = 0;

                    

                    for (; i < columnList.getLength() + k; i++) {
                        fieldNamesFromXML[i] = ((Element)columnList.item(j)).getAttribute("name");
                        dataTypesFromXML[i] = ((Element)columnList.item(j)).getAttribute("type");
                        tableNamesFromXML[i] = eElement.getAttribute("name");
                       

//                        System.out.println("((Element)columnList.item(j)).getAttribute(name): " +fieldNamesFromXML[i]);
//                        System.out.println("dataType: " +((Element)columnList.item(j)).getAttribute("type"));
//                        System.out.println("name: "+eElement.getAttribute("name"));
                        j++;
                    }
                    k += columnList.getLength();
                }
            }

            statement = connection.createStatement();
            statement.executeUpdate("UPDATE chin_remote_metadatafields SET df_isapproved = false;");
            
            statement = connection.createStatement();


            for (i = 0; i < numberOfColumnsXML; i++) {
                for (int j = 0; j < numberOfColumnsDB; j++) {
//                    System.out.println(fieldNamesFromXML[i] + " " + fieldNamesFromDatabase[j] + " " + tableNamesFromXML[i] + " " + tableNamesFromDatabase[j]);
                    if (fieldNamesFromXML[i].equalsIgnoreCase(fieldNamesFromDatabase[j])) {
                        if (tableNamesFromXML[i].equalsIgnoreCase(tableNamesFromDatabase[j])) {
                            statement = connection.createStatement();
                            resultSet = statement.executeQuery("SELECT table_id FROM chin_remote_tables_ui WHERE table_name=\'" + tableNamesFromDatabase[j] + "\';");
                            resultSet.next();
                            int tableNumber = resultSet.getInt(1);
                            statement.executeUpdate("UPDATE chin_remote_metadatafields SET df_isapproved=TRUE WHERE sql_extract_field_name=\'" + fieldNamesFromDatabase[j] + "\' AND sql_extract_table_id = " + tableNumber);
                            break;
                        }
                    }
                    if (j == (numberOfColumnsDB - 1)) {
                        statement = connection.createStatement();
                        resultSet = statement.executeQuery("SELECT table_id FROM chin_remote_tables_ui WHERE table_name=\'" + tableNamesFromXML[i] + "\';");
                        int sqlType = 0;
                        if(dataTypesFromXML[i].equalsIgnoreCase("integer") ||
							dataTypesFromXML[i].equalsIgnoreCase("int") ||
							dataTypesFromXML[i].equalsIgnoreCase("int4") ||
							dataTypesFromXML[i].equalsIgnoreCase("smallint")||
							dataTypesFromXML[i].equalsIgnoreCase("tinyint")||
							dataTypesFromXML[i].equalsIgnoreCase("int identity") ||
							dataTypesFromXML[i].equalsIgnoreCase("counter")){
                            sqlType = 5;
                        }
                        else if(dataTypesFromXML[i].equalsIgnoreCase("character varying") ||
                                dataTypesFromXML[i].equalsIgnoreCase("text") ||
								dataTypesFromXML[i].equalsIgnoreCase("ntext") ||
                                dataTypesFromXML[i].equalsIgnoreCase("character") ||
                                dataTypesFromXML[i].equalsIgnoreCase("varchar") ||
                                dataTypesFromXML[i].equalsIgnoreCase("longchar") ||
								dataTypesFromXML[i].equalsIgnoreCase("nvarchar") ||
								dataTypesFromXML[i].equalsIgnoreCase("nvarchar2") ||
								dataTypesFromXML[i].equalsIgnoreCase("nchar") ||
								dataTypesFromXML[i].equalsIgnoreCase("char") ||
								dataTypesFromXML[i].equalsIgnoreCase("varchar2") ||
								dataTypesFromXML[i].equalsIgnoreCase("raw")){
                            sqlType = 12;
                        }
                        else if(dataTypesFromXML[i].equalsIgnoreCase("boolean") ||
								dataTypesFromXML[i].equalsIgnoreCase("bit")){
                            sqlType = -7;
                        }
                        else if(dataTypesFromXML[i].equalsIgnoreCase("bigint")){
                            sqlType = 4;
                        }
                        else if(dataTypesFromXML[i].equalsIgnoreCase("datetime") ||
								dataTypesFromXML[i].equalsIgnoreCase("smalldatetime") ||
								dataTypesFromXML[i].equalsIgnoreCase("datetime2") ||
                                dataTypesFromXML[i].equalsIgnoreCase("timestamp") ||
								dataTypesFromXML[i].equalsIgnoreCase("timestamp(6)")){
                            sqlType = 93;
                        }
                        else if(dataTypesFromXML[i].equalsIgnoreCase("double")){
                            sqlType = 8;
                        }
                        else if(dataTypesFromXML[i].equalsIgnoreCase("decimal") ||
                                dataTypesFromXML[i].equalsIgnoreCase("numeric")  ||
								dataTypesFromXML[i].equalsIgnoreCase("number") ||
								dataTypesFromXML[i].equalsIgnoreCase("real") ||
								dataTypesFromXML[i].equalsIgnoreCase("float")){
                            sqlType = 2;
                        }
                        else if(dataTypesFromXML[i].equalsIgnoreCase("date")){
                            sqlType = 91;
                        }
                        
                        if (resultSet.next()) {
                            int tableNumber = resultSet.getInt(1);
                            statement.executeUpdate("INSERT INTO chin_remote_metadatafields (df_translator_field_sqltype, df_mdataid, df_name, df_type, sql_extract_dataset_id, sql_extract_field_name, sql_extract_table_id) VALUES(" + sqlType + ", " + (++orderNumber) + ", \'" + fieldNamesFromXML[i] + "\', \'" + dataTypesFromXML[i] + "\', " + sqlExtractDatasetID + ", \'" + fieldNamesFromXML[i] + "\', " + tableNumber + ");");
                            //System.out.println(tableNamesFromXML[i] + " " + fieldNamesFromXML[i]);
                        }else{
							
//                            if(tableNamesFromXML[i].equals(persontable)){
//                            	statement.executeUpdate("INSERT INTO chin_remote_tables_ui(table_name, is_persons_tbl) VALUES (\'" + tableNamesFromXML[i] + "\', \'true\');");
//                            }
//                            else{
//                            	statement.executeUpdate("INSERT INTO chin_remote_tables_ui(table_name, is_persons_tbl) VALUES (\'" + tableNamesFromXML[i] + "\', \'false\');");
//                            }
                        	//System.out.println("tableNamesFromXML[i]: "+tableNamesFromXML[i]);
                            resultSet = statement.executeQuery("SELECT table_id FROM chin_remote_tables_ui WHERE table_name=\'" + tableNamesFromXML[i] + "\';");
                            try{
                            resultSet.next();
                            int tableNumber = resultSet.getInt(1);
                            statement.executeUpdate("INSERT INTO chin_remote_metadatafields (df_translator_field_sqltype, df_mdataid, df_name, df_type, sql_extract_dataset_id, sql_extract_field_name, sql_extract_table_id) VALUES(" + sqlType + ", " + (++orderNumber) + ", \'" + fieldNamesFromXML[i] + "\', \'" + dataTypesFromXML[i] + "\', " + sqlExtractDatasetID + ", \'" + fieldNamesFromXML[i] + "\', " + tableNumber + ");");
                            }
                            catch (PSQLException e){
                            	//e.printStackTrace();
                            }
                        }
                    }
                }
            }
            connection.close();
        }
    } catch (SQLException e) {
        System.out.println("SQLException");
        e.printStackTrace();
    } catch (XPathExpressionException e) {
        System.out.println("XPathExpressionException");
        e.printStackTrace();
    } catch (IOException e) {
        System.out.println("IOException");
        e.printStackTrace();
    } catch (ParserConfigurationException e) {
        System.out.println("ParserConfigurationException");
        e.printStackTrace();
    } catch (SAXException e) {
        System.out.println("SAXException");
        e.printStackTrace();
    }
	}
}

